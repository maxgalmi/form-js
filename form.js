var textNodes = [
	document.createTextNode('Le nom doit avoir 2 caractères au minimum'),
	document.createTextNode('Le prénom doit avoir 2 caractères au minimum'),
	document.createTextNode("L'age doit être un nombre compris entre 5 et 140"),
	document.createTextNode("Le pseudo doit contenir au moins 4 caractères"),
	document.createTextNode("Les 2 mots de passes doivent être identiques et non vides"),
	document.createTextNode("Veuillez entrer une adresse email valide")
		]
	
function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

var nom = document.getElementById('nom')
var male = document.getElementById('male')
var female = document.getElementById('female')
var prenom = document.getElementById('prenom')
var age = document.getElementById('age')
var pseudo = document.getElementById('pseudo')
var mdp = document.getElementById('mdp'),
    mdp2 = document.getElementById('mdp2');
var mail = document.getElementById('mail')
var form = document.getElementById('form');
var ville  = document.getElementById('ville');

nom.addEventListener("keyup", function(){
  if(nom.value.length < 2){
    nom.className = 'red'
      insertAfter(textNodes[0],nom);
  }else{
    nom.className = 'green'
    nom.parentNode.removeChild(textNodes[0]);  
  }
  
}
);


prenom.addEventListener("keyup", function(){
  if(prenom.value.length < 2){
      prenom.className = 'red'
      insertAfter(textNodes[1],prenom);
  }else{
    prenom.className = 'green'
    prenom.parentNode.removeChild(textNodes[1]);  
  }
}
);



age.addEventListener("keyup", function(){
  var a = parseInt(age.value)
  if(isNaN(a)||a<5||a>140){
     age.className = 'red'
      insertAfter(textNodes[2],age);
  }else{
    age.parentNode.removeChild(textNodes[2]); 
    age.className = 'green'
  }
}
);


pseudo.addEventListener("keyup", function(){
  if(pseudo.value.length < 4){
      pseudo.className = 'red'
      insertAfter(textNodes[3],pseudo);
  }else{
   pseudo.parentNode.removeChild(textNodes[3]);  
   pseudo.className = 'green'
  }
}
);



var compare = function(){
	if (mdp.value && mdp.value == mdp2.value){
		mdp.className = 'green'
		mdp2.className = 'green'
		mdp.parentNode.removeChild(textNodes[4]);
	}
	else{
		mdp.className = 'red'
		mdp2.className = 'red'
		insertAfter(textNodes[4],mdp);
	}

};

mdp.addEventListener("keyup", compare);
mdp2.addEventListener("keyup", compare);



mail.addEventListener("keyup", function(){
  if(/^[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]+/.test(mail.value)){
      mail.parentNode.removeChild(textNodes[5]);  
      mail.className = 'green'
    
  }else{
      mail.className = 'red'
      insertAfter(textNodes[5],mail);
   
  }
}
);



form.addEventListener('submit', function(e) {
	  var a = parseInt(age.value)
	  if(!male.checked && !female.checked)
	    alert('Vous devez selectionner un sexe');
	  else if(nom.value.length < 2)
	    alert('Le nom doit avoir 2 caractères au minimum');
	  else if(prenom.value.length < 2)
	    alert('Le prénom doit avoir 2 caractères au minimum');
	  else if(isNaN(a)||a<5||a>140)
	    alert("L'age doit être un nombre compris entre 5 et 140");
	  else if(pseudo.value.length < 4)
	    alert("Le pseudo doit contenir au moins 4 caractères");
	  else if (!mdp.value || mdp.value != mdp2.value  )
	    alert("Les 2 mots de passes doivent être identiques et non vides")
	  else if(!/^[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]+/.test(mail.value))
	    alert("Veuillez entrer une adresse email valide")
	   else if(ville.value.length < 1)
	    alert("Veuillez entrer une ville");
	  else
	    alert('Tout est correct');

        e.preventDefault();

 });

  
    
	

  

